package com.example.btbspring03.controllers;

import com.example.btbspring03.models.Book;
import com.example.btbspring03.models.Category;
import com.example.btbspring03.service.BookService;
import com.example.btbspring03.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Controller
public class BookController {
    private BookService bookService;
    private CategoryService categoryService;

    @Autowired
    public BookController(BookService bookService,CategoryService categoryService ) {
        this.bookService = bookService;
        this.categoryService = categoryService;
    }

    @GetMapping("/index")
    public String index(Model model){
        List<Book> bookList = this.bookService.getData();
        model.addAttribute("books",bookList);
        return "index";
    }

    @GetMapping("view/{id}")
    public String view(@PathVariable Integer id,Model model){
        System.out.println("ID: "+id);
        Book book = this.bookService.view(id);
        model.addAttribute("books",book);
        return "view";
    }

    @GetMapping("/update/{book_id}")
    public String update(@PathVariable Integer book_id,Model model){
        List<Category> categories = this.categoryService.getAll();
        System.out.println("Update"+book_id);
        Book book = this.bookService.view(book_id);
        model.addAttribute("books",book);
        model.addAttribute("categories",categories);
        return "update";
    }

    @PostMapping("update/submit")
    public String updateSubmit(@ModelAttribute Book book,Model model,MultipartFile file){
        System.out.println(book);
        model.addAttribute("books",book);

        File part = new File("/btb");

        if(!part.exists())
            part.mkdir();

        String filename = file.getOriginalFilename();
        String extendsion = filename.substring(filename.lastIndexOf(".")+1);
        System.out.println(filename);
        System.out.println(extendsion);

        filename = UUID.randomUUID()+"."+ extendsion;
        try{
            Files.copy(file.getInputStream(), Paths.get("/btb",filename));
        }catch (IOException e){
            System.out.println(e);
        }

        if(!file.isEmpty()){
            book.setThumbnail("/images-btb/"+filename);
        }

        bookService.update(book);
        return "redirect:/index";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer id,Model model){
        System.out.println(id);
        bookService.delete(id);
        return "redirect:/index";
    }

    @GetMapping("/create")
    public String create(Model model){
        List<Category> categories = this.categoryService.getAll();
        model.addAttribute("book",new Book());
        model.addAttribute("categories",categories);
        return "createnew";
    }

    @PostMapping("/create/submit")
    public String create(@Valid Book book, BindingResult bindingResult, MultipartFile file){

        if(file==null){
            return null;
        }
        File part = new File("/btb");

        if(!part.exists())
            part.mkdir();

        String filename = file.getOriginalFilename();
        String extendsion = filename.substring(filename.lastIndexOf(".")+1);
        System.out.println(filename);
        System.out.println(extendsion);

        filename = UUID.randomUUID()+"."+ extendsion;
        try{
            Files.copy(file.getInputStream(), Paths.get("/btb",filename));
        }catch (IOException e){
            System.out.println(e);
        }
        book.setThumbnail("/images-btb/"+filename);

        if(bindingResult.hasErrors()){
            System.out.println(book);
            return "createnew";
        }
        System.out.println(book);
        bookService.create(book);
        return "redirect:/index";
    }

}
