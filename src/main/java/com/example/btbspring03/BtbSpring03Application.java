package com.example.btbspring03;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BtbSpring03Application {

    public static void main(String[] args) {
        SpringApplication.run(BtbSpring03Application.class, args);
		//SpringApplication.run(BtbSpring03Application.class, args);
    }
}
