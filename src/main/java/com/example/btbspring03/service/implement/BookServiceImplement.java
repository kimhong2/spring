package com.example.btbspring03.service.implement;

import com.example.btbspring03.models.Book;
import com.example.btbspring03.repositories.BookRepositoty;
import com.example.btbspring03.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImplement implements BookService {

    private BookRepositoty bookRepositoty;

    @Autowired
    public BookServiceImplement(BookRepositoty bookRepositoty) {
        this.bookRepositoty = bookRepositoty;
    }

    @Override
    public List<Book> getData() {
        return this.bookRepositoty.getData();
    }

    @Override
    public Book view(Integer id) {
        return this.bookRepositoty.view(id);
    }

    @Override
    public boolean update(Book book) {
        return this.bookRepositoty.update(book);
    }

    @Override
    public boolean delete(Integer id) {
        return this.bookRepositoty.delete(id);
    }

    @Override
    public boolean create(Book book) {
        return this.bookRepositoty.create(book);
    }
}
