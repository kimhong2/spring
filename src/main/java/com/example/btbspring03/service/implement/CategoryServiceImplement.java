package com.example.btbspring03.service.implement;

import com.example.btbspring03.models.Category;
import com.example.btbspring03.repositories.CategoryRepository;
import com.example.btbspring03.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CategoryServiceImplement implements CategoryService {
    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImplement(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAll(){
        return categoryRepository.getAll();
    }
}
