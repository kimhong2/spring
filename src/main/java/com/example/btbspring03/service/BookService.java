package com.example.btbspring03.service;

import com.example.btbspring03.models.Book;

import java.util.List;

public interface BookService {
    List<Book> getData();
    Book view(Integer id);
    boolean update(Book book);
    boolean delete(Integer id);
    boolean create(Book book);
}
