package com.example.btbspring03.repositories;

import com.example.btbspring03.models.Category;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {
    @Select("Select * from tb_category")
    List<Category> getAll();
}

