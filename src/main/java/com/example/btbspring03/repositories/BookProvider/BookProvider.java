package com.example.btbspring03.repositories.BookProvider;

import com.example.btbspring03.models.Book;
import org.apache.ibatis.jdbc.SQL;

public class BookProvider {
    public String getDataProvider(){
        return new SQL(){{
            SELECT("*");
            FROM("tb_book b");
            INNER_JOIN("tb_category c ON b.cate_id = c.id");
            ORDER_BY("b.id asc");
        }}.toString();
    }

    public String createProvider(Book book){
        return new SQL(){{
            INSERT_INTO("tb_book");
            VALUES("title","#{title}");
            VALUES("author","#{author}");
            VALUES("publisher","#{publisher}");
            VALUES("thumbnail","#{thumbnail}");
            VALUES("cate_id","#{category.id}");

        }}.toString();
    }
}
