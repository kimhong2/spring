create table tb_category(
	id serial primary key,
	name VARCHAR
);
CREATE table tb_book(
	id serial PRIMARY key,
	title VARCHAR,
	author VARCHAR,
	publisher VARCHAR,
	thumbnail VARCHAR,
	cate_id int references tb_category(id)
);


